<?php
	/**
	* URL Parser for the Router (also for MVC)
	*/
	class UrlParser
	{
		private $url;

		function __construct($url = null)
		{
			$this->url = !is_null($url) ? $url : $this->getURL();
			// $this->getParams();
			// $this->getQuery();
		}

		public function getURL()
		{
			$url = isset($_SERVER['HTTPS']) ? 'https://' : 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'];
			return $url;
		}

		public function getParams()
		{
			# Get url
			$url = parse_url($this->getURL(), PHP_URL_PATH);

			# Parse the URL to only keep params
			$parsedurl = str_replace(WEBROOT, "", $url);
			
			# Split the parsed URL to get params
			$params = split("/", $parsedurl);
			$params = !empty($params[0]) ? $params : NULL;

			return $params;
		}
	}
	// $request = str_replace(WEBROOT, "", $_SERVER['REQUEST_URI']);

	// $params = split("/", $request);
	// $params = !empty($params[0]) ? $params : NULL;
?>